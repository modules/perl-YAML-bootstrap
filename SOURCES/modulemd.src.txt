document: modulemd
version: 2
data:
    stream: '1.24'
    summary: Helper module for boostrapping YAML
    description: >
        This module is not intended for general use. It's a helper module for
        bootstrapping YAML module.
    license:
        module: [ MIT ]
    dependencies:
        - buildrequires:
              platform: [el8]
              perl: []
          requires:
              platform: [el8]
              perl: []
    references:
        community: https://metacpan.org/release/YAML
    buildopts:
        rpms:
            macros: |
                %perl_bootstrap 1
                %_with_perl_YAML_enables_test 1
                %_without_perl_Test_Base_enables_diff 1
                %_without_perl_Test_Base_enables_extra_test 1
                %_without_perl_Test_Base_enables_network 1
    components:
        rpms:
            perl-YAML:
                rationale: build dependency.
                ref: stream-1.24-rhel-8.3.0
                buildorder: 0
            perl-Spiffy:
                rationale: build dependency.
                ref: stream-0.46-rhel-8.3.0
                buildorder: 1
            perl-Test-Deep:
                rationale: build dependency.
                ref: stream-1.127-rhel-8.3.0
                buildorder: 2
            perl-Test-Base:
                rationale: build dependency.
                ref: stream-0.89-rhel-8.3.0
                buildorder: 3
            perl-Test-YAML:
                rationale: build dependency.
                ref: stream-1.06-rhel-8.3.0
                buildorder: 4
